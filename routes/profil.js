var express = require('express');
var router = express.Router();
var connection= require('../lib/db');

/* GET home page. */

//GET PROFIL
router.get('/', function(req, res, next) {
      
 connection.query('SELECT * FROM ROLE',function(err,rows)     {
 
        if(err){
         req.flash('error', err); 
         res.render('profil',{page_title:"Profil",data:''});   
        }else{
            
            res.render('profil',{page_title:"Profil",data:rows});
        }
                            
         });
        
});

//ADD PROFL
// SHOW ADD USER FORM
router.get('/add', function(req, res, next){    
    // render to views/user/add.ejs
    res.render('profil/add', {
        title: 'Add New profil',
        LIBELLE: ''        
    })
})
 
// ADD NEW USER POST ACTION
router.post('/add', function(req, res, next){    
    //req.assert('LIBELLE', 'libelle is required').notEmpty()           //Validate name
  
   // var errors = req.validationErrors()
     
   // if( !errors ) {   //No errors were found.  Passed Validation!
         
     
       var libelle = {
           libelle: req.sanitize('LIBELLE').escape().trim(),
        }
         
     connection.query('INSERT INTO ROLE SET ?', libelle, function(err, result) {
                //if(err) throw err
                if (err) {
                    //req.flash('error', err)
                     
                    // render to views/user/add.ejs
                    res.render('profil/add', {
                        title: 'Add New Customer',
                        LIBELLE: libelle.LIBELLE                   
                    })
                } else {                
                    //req.flash('success', 'Profil enregistré avec succès!');
                    res.redirect('/profil');
                }
            });
   // }
    //else {   //Display errors to user
      //  var error_msg = ''
       // errors.forEach(function(error) {
        //    error_msg += error.msg + '<br>'
       // })                
        //req.flash('error', error_msg)        
         
        /**
         * Using req.body.name 
         * because req.param('name') is deprecated
         */ 
       /// res.render('profil/add', { 
         //   title: 'Add New profil',
        //    LIBELLE: req.body.LIBELLE
       // })
   // }
})
module.exports = router;
